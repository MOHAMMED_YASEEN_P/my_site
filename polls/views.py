from django.shortcuts import render,get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.db.models import F
from django.utils import timezone

from .models import Question, Choice, Tag

# Create your views here.
class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        return Question.objects.prefetch_related('tags').filter(
            pub_date__lte=timezone.now()
            ).order_by('-pub_date')[:5]

class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'

    def get_queryset(self):
        return Question.objects.filter(pub_date__lte=timezone.now())


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'

def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
            })
    else:
       selected_choice.votes = F('votes')+ 1
       selected_choice.save() 
    return HttpResponseRedirect(reverse('polls:result',args=(question_id,)))

# class AddQuestionView(generic.DetailView):
#     model = Question
#     template_name = 'polls/detail.html' 

def new_question(request):
    return render(request, 'polls/new-question.html')

def add_question(request):
    tags = [request.POST[key] for key in request.POST.keys() if key.startswith("tag")]
    # print(tags)
    # tag = request.POST["tag1"]
    question = request.POST["question"]
    choice1 = request.POST["choice1"]
    choice2 = request.POST['choice2']

    question = Question(question_text=question)
    question.save()
    Choice.objects.create(question=question, choice_text=choice1)
    Choice.objects.create(question=question, choice_text=choice2)
    for tag in tags:
        question.tags.create(name=tag)
    return HttpResponseRedirect(reverse('polls:index'))

    # print("****************************************")
    # print(tag,question,choice1,choice2)
    # print("****************************************")

def questions_of_tag(request, tag_id):
    return render(request,'polls/questions_of_tag.html', {'tag':Tag.objects.prefetch_related('questions').get(pk=tag_id)})