from django.urls import path
from . import views

app_name = 'polls'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name='result'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
    path('new-question/',views.new_question, name='new_question'),
    path('add_question',views.add_question,name="add_question"),
    path('tagged/<int:tag_id>',views.questions_of_tag,name="tagged"),

]